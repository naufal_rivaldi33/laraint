const Ziggy = {"url":"http:\/\/localhost","port":null,"defaults":{},"routes":{"pages.home":{"uri":"home","methods":["GET","HEAD"]},"pages.about":{"uri":"about","methods":["GET","HEAD"]}}};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    Object.assign(Ziggy.routes, window.Ziggy.routes);
}

export { Ziggy };
