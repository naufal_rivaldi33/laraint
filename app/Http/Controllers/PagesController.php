<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Inertia\Inertia;

class PagesController extends Controller
{
    public function index()
    {
        $data['title'] = 'Inertia';
        return Inertia::render('Home', $data);
    }

    public function about()
    {
        $data['title'] = 'About';
        return Inertia::render('About', $data);
    }
}
