<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

use Inertia\Inertia;

class LoginController extends Controller
{
    public function index()
    {
        return Inertia::render('Login/Index');
    }

    public function signup(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->route('pages.home');
        }

        return back()->with('danger', 'The provided credentials do not match our records.');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login.index');
    }
}
